pragma Ada_2012;
package body resolutions is

   -----------------------
   -- estChiffreValable --
   -----------------------

   function estChiffreValable
     (g : in Type_Grille; v : Integer; c : Type_Coordonnee) return Boolean
   is
      ca,co,l: Type_Ensemble;
   begin
      if caseVide(g,c) = False then
         raise CASE_NON_VIDE;
      end if;
      ca:=obtenirChiffresDUnCarre(g,(obtenirCarre(c)));
      co:=obtenirChiffresDUneColonne(g,(obtenirColonne(c)));
      l:=obtenirChiffresDUneLigne(g,(obtenirLigne(c)));
      return not appartientChiffre(ca,v) and  not appartientChiffre(co,v) and not appartientChiffre(l,v);
   end estChiffreValable;

   -------------------------------
   -- obtenirSolutionsPossibles --
   -------------------------------

   function obtenirSolutionsPossibles
     (g : in Type_Grille; c : in Type_Coordonnee) return Type_Ensemble
   is
      result: Type_Ensemble;
   begin
      if caseVide(g,c) = False then
         raise CASE_NON_VIDE;
      end if;
      result:=construireEnsemble ;
      for i in Integer range 1..DIMENSION loop
         if estChiffreValable(g,i,c) and not appartientChiffre(result,i) then
            ajouterChiffre(result,i);
         end if;
      end loop ;
      return result;
   end obtenirSolutionsPossibles;

   ------------------------------------------
   -- rechercherSolutionUniqueDansEnsemble --
   ------------------------------------------

   function rechercherSolutionUniqueDansEnsemble
     (resultats : in Type_Ensemble) return Integer
   is
      sol:Integer;
   begin
      if nombreChiffres(resultats) > 1 then
         raise PLUS_DE_UN_CHIFFRE;
      end if ;
      if ensembleVide(resultats) then
         raise ENSEMBLE_VIDE;
      end if ;
       for i in Integer range 1 .. DIMENSION loop
         if appartientChiffre (resultats, i) then
            sol := i;
         end if;
      end loop;

      return sol;

   end rechercherSolutionUniqueDansEnsemble;

   -------------------
   -- case suivante --
   -------------------

   procedure caseSuivante ( c : in out Type_Coordonnee ) is
   begin
       if obtenirColonne(c) = DIMENSION and obtenirLigne(c) < DIMENSION then
               c := construireCoordonnees(obtenirLigne(c)+1,1);
            elsif obtenirColonne(c) = DIMENSION and obtenirLigne(c) = DIMENSION then
               c := construireCoordonnees(1,1);
            else
               c:= construireCoordonnees(obtenirLigne(c),obtenirColonne(c)+1);
         end if ;

   end caseSuivante;
   --------------------
   -- resoudreSudoku --
   --------------------

   procedure resoudreSudoku (g      : in out Type_Grille;
                             trouve :    out Boolean ;
                             cpt : out Integer) is
      pragma Unreferenced (trouve);
      value,try : Integer;
      save:Type_Pile_Grille;
      choix:Type_Pile;
      c,cChoix:Type_Coordonnee;
      mc:Type_Ensemble;
   begin
      try:=0;
      cpt:=0;
      c:=construireCoordonnees(1,1);
      save:=construirePilleGrille;
      while not estRemplie(g) loop
         if try<=(DIMENSION*DIMENSION) then
            if caseVide(g,c) then
               mc := construireEnsemble;
               mc := obtenirSolutionsPossibles(g,c);
               if nombreChiffres(mc) = 1 then
                  value := rechercherSolutionUniqueDansEnsemble(mc);
                  fixerChiffre(g,c,value,cpt);
                  try:=0;
               else
                  try:=try+1;
               end if ;
            end if ;
         elsif try>=(DIMENSION*DIMENSION) and try < (2*(DIMENSION*DIMENSION)) then
             if caseVide(g,c) then
               mc := construireEnsemble;
               mc := obtenirSolutionsPossibles(g,c);
               if nombreChiffres(mc) = 2 then
                  save:=empileGrille(save,g);
                  choix:=empilerValeurPossible(mc);
                  try:=0;
                  cChoix:=c;
                  fixerChiffre(g,cChoix,dernier(choix),cpt);
               else
                  try:=try+1;
               end if ;
            end if ;
         else
            g:=dernierGrille(depileGrille(save));
            fixerChiffre(g,cChoix,dernier(depile(choix)),cpt);
         end if ;
         caseSuivante(c);
      end loop;
      trouve:=estRemplie(g);
      afficherGrille(g);
      put(cpt);
   end resoudreSudoku;

end resolutions;
