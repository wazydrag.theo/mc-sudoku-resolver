with Ada.Text_IO; use ada.Text_IO;
with TAD_grilleSudoku; use TAD_grilleSudoku;
package TAD_Pile_Grille is

   --d�claration des exceptions
   PILE_GRILLE_PLEINE:exception;
   PILE_GRILLE_VIDE:exception;
   
   --d�claration de la constante MAX_LENGTH
   MAX_LENGTH:constant Integer := 255;
   
   --d�claration du type Pile_Grille en priv�
   type Type_Pile_Grille is private;
   
   function construirePilleGrille return Type_Pile_Grille;
   
   function empileGrille(pg:in Type_Pile_Grille; g:in Type_Grille) return Type_Pile_Grille;
   
   function depileGrille(pg:in Type_Pile_Grille) return Type_Pile_Grille;
   
   function estVideGrille(pg:in Type_Pile_Grille) return Boolean;
   
   function dernierGrille(pg:in Type_Pile_Grille) return Type_Grille;
   
private
   type TabloGrille is array (1..MAX_LENGTH) of Type_Grille;
   type Type_Pile_Grille is record
      tab_grille:TabloGrille;
      nb_grille:integer;
   end record;

end TAD_Pile_Grille;
