package body TAD_Pile_Grille is

   ---------------------------
   -- construirePilleGrille --
   ---------------------------

   function construirePilleGrille return Type_Pile_Grille is
      pg:Type_Pile_Grille;
   begin
      pg.nb_grille:=0;
      return pg;
   end construirePilleGrille;

   ------------------
   -- empileGrille --
   ------------------

   function empileGrille
     (pg:in Type_Pile_Grille;
      g:in Type_Grille)
      return Type_Pile_Grille
   is
      newPg:Type_Pile_Grille;
      i:Type_Grille;
   begin
      if pg.nb_grille=MAX_LENGTH then
         raise PILE_GRILLE_PLEINE;
      end if;

      newPg:=pg;
      i:=g;
      newPg.nb_grille:=newPg.nb_grille+1;
      newPg.tab_grille(newPg.nb_grille):=i;
      return newPg;
   end empileGrille;

   ------------------
   -- depileGrille --
   ------------------

   function depileGrille (pg:in Type_Pile_Grille) return Type_Pile_Grille is
      newPg:Type_Pile_Grille;
   begin
      if pg.nb_grille=0 then
         raise PILE_GRILLE_VIDE;
      end if;
      newPg:=pg;
      newPg.nb_grille:=newPg.nb_grille-1;
      return newPg;
   end depileGrille;

   -------------------
   -- estVideGrille --
   -------------------

   function estVideGrille (pg:in Type_Pile_Grille) return Boolean is
   begin
      return(pg.nb_grille=0);
   end estVideGrille;

   -------------------
   -- dernierGrille --
   -------------------

   function dernierGrille (pg:in Type_Pile_Grille) return Type_Grille is
   begin
      if pg.nb_grille=0 then
         raise PILE_GRILLE_VIDE;
      end if;
      return pg.tab_grille(pg.nb_grille);
   end dernierGrille;

end TAD_Pile_Grille;
