package tad_pile_entier is

   --declaration des exceptions
   PILE_PLEINE : exception;
   PILE_VIDE : exception;

   --d�claration de la constante TAILLE_MAX � une valeur de 13
   TAILLE_MAX: constant Integer := 13;

   --d�claration du tableau d'entier de type tablo
   type tablo is array (1..TAILLE_MAX) of integer;

   --declaration du type pile en priv�
   type Type_Pile is private;

   -- Generateur de base, construit un type pile
   function construirePile return Type_Pile;

   -- Generateur de base, ajoute un entier e � la pile
   function empile (p: in Type_Pile; e: in integer) return Type_Pile;

   -- Generateur secondaire, enl�ve un entier � la pile p ssi la pile n'est pas vide
   function depile (p: in Type_Pile) return Type_Pile;

   --observateur, retourne vrai si la pile p est vide, et faux sinon
   function estVide (p: in Type_Pile) return boolean;

   --observateur, retourne la derni�re valeur de la pile p ssi la pile n'est pas vide
   function dernier (p: in Type_Pile) return integer;

private
   -- creation du type pile
   type Type_Pile is record
      nb: Integer;
      tab: tablo;
   end record;

end tad_pile_entier;
