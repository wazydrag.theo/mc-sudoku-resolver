package body tad_pile_entier is

   function construirePile return Type_Pile is
      p: Type_Pile;
   begin
      p.nb:=0;
      return p;
   end construirePile;

   function estVide (p: in Type_Pile) return boolean is
   begin
   return(p.nb=0);
   end estVide;

   function dernier (p: in Type_Pile) return integer is
   begin

      if p.nb=0 then
         raise PILE_VIDE;
      end if;

   return p.tab(p.nb);
   end dernier;

   function empile (p: in Type_Pile; e: in integer) return Type_Pile is
      nouvelle_pile: Type_Pile;
      i: integer;
   begin

      if p.nb=TAILLE_MAX then
         raise PILE_PLEINE;
      end if;

      nouvelle_pile:=p;
      i:=e;
      nouvelle_pile.nb:=nouvelle_pile.nb+1;
      nouvelle_pile.tab(nouvelle_pile.nb):=i;
      return nouvelle_pile;
   end empile;

   function depile (p: in Type_Pile) return Type_Pile is
      nouvelle_pile:Type_Pile;
   begin

      if p.nb=0 then
         raise PILE_VIDE;
      end if;

      nouvelle_pile:=p;

      nouvelle_pile.nb:=nouvelle_pile.nb-1;

      return nouvelle_pile;
   end depile;

end tad_pile_entier;
