pragma Ada_2012;
package body TAD_grilleSudoku is

   ----------------------
   -- construireGrille --
   ----------------------

   function construireGrille return Type_Grille is
      grille : Type_Grille;
   begin
      for a in Integer range 1..DIMENSION loop
         for b in Integer range 1..DIMENSION loop
            grille(a,b):=0;
         end loop ;
      end loop ;
      return grille ;
   end construireGrille;

   --------------
   -- caseVide --
   --------------

   function caseVide
     (g : in Type_Grille; c : in Type_Coordonnee) return Boolean
   is
   begin
      return g(obtenirLigne(c),obtenirColonne(c)) = 0;
   end caseVide;

   --------------------
   -- obtenirChiffre --
   --------------------

   function obtenirChiffre
     (g : in Type_Grille; c : in Type_Coordonnee) return Integer
   is
      li : Integer := obtenirLigne(c);
      col : Integer := obtenirColonne(c);
   begin
      if  g(li,col) = 0 then
         raise OBTENIR_CHIFFRE_NUL;
      end if ;
      return g(li,col);
   end obtenirChiffre;

   --------------------
   -- nombreChiffres --
   --------------------

   function nombreChiffres (g : in Type_Grille) return Integer is
      nb: Integer;
   begin
      nb:=0;
      for i in Integer range 1 .. DIMENSION loop
         for v in Integer range 1 .. DIMENSION loop
            if g(i,v) /= 0 then
               nb:=nb+1;
            end if ;
         end loop;
      end loop ;
      return nb;
   end nombreChiffres;

   ------------------
   -- fixerChiffre --
   ------------------

   procedure fixerChiffre
     (g : in out Type_Grille;
      c : in     Type_Coordonnee;
      v : in     Integer;
      cpt : in out Integer)
   is
      li : Integer := obtenirLigne(c);
      col : Integer := obtenirColonne(c);
   begin
      if g(li,col) /= 0 then
         raise FIXER_CHIFFRE_NON_NUL;
      end if ;
      g(li,col):=v;
      cpt:=cpt+1;
   end fixerChiffre;

   ---------------
   -- viderCase --
   ---------------

   procedure viderCase (g : in out Type_Grille; c : in out Type_Coordonnee) is
      li : Integer := obtenirLigne(c);
      col : Integer := obtenirColonne(c);
   begin
      if g(li,col)=0 then
         raise VIDER_CASE_VIDE;
      end if ;
      g(li,col):=0;
   end viderCase;

   ----------------
   -- estRemplie --
   ----------------
   function estRemplie (g : in Type_Grille) return Boolean is
   begin
      return nombreChiffres(g) = DIMENSION*DIMENSION ;
   end estRemplie;

   ------------------------------
   -- obtenirChiffresDUneLigne --
   ------------------------------

   function obtenirChiffresDUneLigne
     (g : in Type_Grille; numLigne : in Integer) return Type_Ensemble
   is
      Ligne : Type_Ensemble;
   begin
      Ligne:=construireEnsemble;
      for i in Integer range 1..DIMENSION loop
         if g(numLigne,i) /= 0 then
            ajouterChiffre(Ligne,g(numLigne,i));
         end if ;
      end loop ;
      return Ligne;
   end obtenirChiffresDUneLigne;

   --------------------------------
   -- obtenirChiffresDUneColonne --
   --------------------------------

   function obtenirChiffresDUneColonne
     (g : in Type_Grille; numColonne : in Integer) return Type_Ensemble
   is
      Colonne : Type_Ensemble;
   begin
      Colonne:=construireEnsemble;
      for i in Integer range 1..DIMENSION loop
         if g(i,numColonne) /= 0 then
            ajouterChiffre(Colonne, g(i,numColonne));
         end if ;
      end loop ;
      return Colonne;
   end obtenirChiffresDUneColonne;

   -----------------------------
   -- obtenirChiffresDUnCarre --
   -----------------------------

   function obtenirChiffresDUnCarre
     (g : in Type_Grille; numCarre : in Integer) return Type_Ensemble
   is
      li : Integer := obtenirLigne(obtenirCoordonneeCarre(numCarre));
      co : Integer := obtenirColonne(obtenirCoordonneeCarre(numCarre));
      carre : Type_Ensemble;
   begin
      carre:=construireEnsemble;
      for i in Integer range 0..2 loop
         for v in Integer range 0..2 loop
             if g(li+i,co+V) /= 0 then
            ajouterChiffre(carre,g(li+i,co+V));
            end if ;
         end loop ;
      end loop ;
      return carre;
   end obtenirChiffresDUnCarre;

end TAD_grilleSudoku;
