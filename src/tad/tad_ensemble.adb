pragma Ada_2012;
package body TAD_ensemble is

   ------------------------
   -- construireEnsemble --
   ------------------------
   function construireEnsemble return Type_Ensemble is
      te:Type_Ensemble;
   begin
      for i in Integer range 1..DIMENSION loop
         te(i):=True;
      end loop ;
      return te ;
   end construireEnsemble;

   ------------------
   -- ensembleVide --
   ------------------
   function ensembleVide (e : in Type_Ensemble) return Boolean is
   begin
      return nombreChiffres(e) = 0;
   end ensembleVide;

   -----------------------
   -- appartientChiffre --
   -----------------------
   function appartientChiffre
     (e : in Type_Ensemble; v : Integer) return Boolean is
   begin
     return not e(v) ;
   end appartientChiffre;

   --------------------
   -- nombreChiffres --
   --------------------
   function nombreChiffres (e : in Type_Ensemble) return Integer is
      occurence:integer:=0;
   begin
      for i in Integer range 1 .. DIMENSION loop
         if e(i) = False then
            occurence:=occurence+1;
         end if;
      end loop;
      return occurence;
   end nombreChiffres;

   --------------------
   -- ajouterChiffre --
   --------------------
   procedure ajouterChiffre (e : in out Type_Ensemble; v : in Integer) is
   begin
      if e(v) = False then
         raise APPARTIENT_ENSEMBLE;
      end if ;
      e(v) := False;
   end ajouterChiffre;

   --------------------
   -- retirerChiffre --
   --------------------
   procedure retirerChiffre (e : in out Type_Ensemble; v : in Integer) is
   begin
      if e(v) = True then
         raise NON_APPARTIENT_ENSEMBLE;
      end if ;
      e(v) := True;
   end retirerChiffre;

    ---------------------------
   -- empilerValeurPossible --
   ---------------------------
   function empilerValeurPossible(e : in Type_Ensemble)return Type_pile is
      p : Type_Pile;
   begin
      if nombreChiffres(e) < 2 then
         raise MOINS_DE_DEUX_VAL;
      end if;
      p:=construirePile;
      for i in Integer range 1..DIMENSION loop
            if not appartientChiffre(e,i) then
               p:=empile(p,i);
            end if;
         end loop;
         return p;
   end empilerValeurPossible;

end TAD_ensemble;
