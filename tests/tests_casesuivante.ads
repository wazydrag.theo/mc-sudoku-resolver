with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with tests;                 use tests;

package tests_casesuivante is

   -- Test CaseSuivante
  function test_casesuivante_P1 return Boolean;
  function test_casesuivante_P2 return Boolean;
  function test_casesuivante_P3 return Boolean;

  les_Tests : constant array (Positive range <>) of Test_Unitaire :=
  ((test_casesuivante_P1'Access,
  To_Unbounded_String ("echec de la propriete 1 Case Suivante")),
  (test_casesuivante_P2'Access,
  To_Unbounded_String ("echec de la propriete 2 Case Suivante")),
  (test_casesuivante_P3'Access,
  To_Unbounded_String ("echec de la propriete 3 Case Suivante")));

   resultats : array (les_Tests'Range) of Boolean;
end tests_casesuivante;
