with ada.Text_IO; use ada.Text_IO;
with ada.Integer_Text_IO;use ada.Integer_Text_IO;
--TAD
with TAD_Pile_Grille;use TAD_Pile_Grille;
with TAD_grilleSudoku;use TAD_grilleSudoku;
with remplirGrille;use remplirGrille;

package body tests_TAD_pile_grille is

   --------------------
   -- test_P1_grille --
   --------------------

   function test_P1_grille return Boolean is
      pg:Type_Pile_Grille;
   begin
      pg:=construirePilleGrille;
      return estVideGrille(pg);
   end test_P1_grille;

   --------------------
   -- test_P2_grille --
   --------------------

   function test_P2_grille return Boolean is
      pg:Type_Pile_Grille;
      g :Type_Grille:= grille1TresTresFacileDuNouvelAn2018;
   begin
      pg:=construirePilleGrille;
      pg:=empileGrille(pg, g);
      return not(estVideGrille(pg));
   end test_P2_grille;

   --------------------
   -- test_P3_grille --
   --------------------

   function test_P3_grille return Boolean is
      pg:Type_Pile_Grille;
      g:Type_Grille:=grille1TresTresFacileDuNouvelAn2018;
   begin
      pg:=construirePilleGrille;
      pg:=empileGrille(pg, g);
      return dernierGrille(pg)=g;
   end test_P3_grille;

   ---------------------
   -- test_P4a_grille --
   ---------------------

   function test_P4a_grille return Boolean is
         pg:Type_Pile_Grille;
         g :Type_Grille:= grille1TresTresFacileDuNouvelAn2018;
   begin
      pg:=construirePilleGrille;
      pg:=empileGrille(pg,g);
      pg:=depileGrille(pg);
      return estVideGrille(pg);
   end test_P4a_grille;

   ---------------------
   -- test_P4b_grille --
   ---------------------

   function test_P4b_grille return Boolean is
      pg:Type_Pile_Grille;
      g:Type_Grille:=grille1TresTresFacileDuNouvelAn2018;
      e:Type_Grille:=grille2TresTresFacile;
   begin
      pg:=construirePilleGrille;
      pg:=empileGrille(pg, g);
      pg:=empileGrille(pg, e);
      pg:=depileGrille(pg);
      return dernierGrille(pg)=g;

   end test_P4b_grille;

   --------------------------------------
   -- test_Exception_PileGrille_Pleine --
   --------------------------------------

   function test_Exception_PileGrille_Pleine return Boolean is
      pg:Type_Pile_Grille;
      g:Type_Grille:= grille1TresTresFacileDuNouvelAn2018;
   begin
      pg:=construirePilleGrille;
      for i in 1..MAX_LENGTH loop
         pg:=empileGrille(pg, g);
      end loop;
      return False;
   exception
      when PILE_GRILLE_PLEINE =>
         return True;
      when others =>
         return False;
   end test_Exception_PileGrille_Pleine;

   -------------------------------------
   -- test_Exception_PileGrille_Videa --
   -------------------------------------

   function test_Exception_PileGrille_Videa return Boolean is
      pg:Type_Pile_Grille;
      g:Type_Grille;
   begin
      pg:=construirePilleGrille;
      g:=dernierGrille(pg);
      return False;
   exception
      when PILE_GRILLE_VIDE =>
         return True;
      when others =>
         return False;
   end test_Exception_PileGrille_Videa;

   -------------------------------------
   -- test_Exception_PileGrille_Videb --
   -------------------------------------

   function test_Exception_PileGrille_Videb return Boolean is
      pg:Type_Pile_Grille;
   begin
      pg:=construirePilleGrille;
      pg:=depileGrille(pg);
      return False;
   exception
      when PILE_GRILLE_VIDE =>
         return True;
      when others =>
         return False;
   end test_Exception_PileGrille_Videb;

end tests_TAD_pile_grille;
