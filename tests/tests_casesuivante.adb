-- bibliothèques d'entrée sortie
with Ada.Text_IO;         use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
-- TAD
with TAD_Coordonnee; use TAD_Coordonnee;
with resolutions; use resolutions;
package body tests_casesuivante is
   
   -------------------------------
   --   tests Case Suivante P1  --
   -------------------------------

   function test_casesuivante_P1 return Boolean is 
      c : Type_Coordonnee;
   begin 
      c:=construireCoordonnees(2,2);
      casesuivante(c);   
      return obtenirLigne(c)=2 and obtenirColonne(c)=3 ;
   end test_casesuivante_P1;
   
   -------------------------------
   --   tests Case Suivante P2  --
   -------------------------------
   
   function test_casesuivante_P2 return Boolean is
      c : Type_Coordonnee;
   begin
      c:=construireCoordonnees(1,9);
         casesuivante(c);
      return obtenirLigne(c)= 2 and obtenirColonne(c)=1;
   end test_casesuivante_P2;
   
   -------------------------------
   --   tests Case Suivant P3   --
   -------------------------------
   
   function test_casesuivante_P3 return Boolean is  
      c : Type_Coordonnee;
   begin
      c:=construireCoordonnees(9,9);
         caseSuivante(c);
      
   return obtenirLigne(c)=1 and obtenirColonne(c) =1 ;
   end test_casesuivante_P3;
   
end tests_casesuivante;
