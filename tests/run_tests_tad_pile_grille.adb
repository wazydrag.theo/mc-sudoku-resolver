with tests_TAD_pile_grille; use tests_TAD_pile_grille;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

procedure run_tests_TAD_Pile_Grille is
begin
   --Execution P1
   if test_P1_grille then
      Put_Line("Test P1 OK");
   else
      Put_line("Test P1 KO");
   end if;
   --Execution P2
   if test_P2_grille then
      Put_Line("Test P2 OK");
   else
      Put_line("Test P2 KO");
   end if;
   --Execution P3
   if test_P3_grille then
      Put_Line("Test P1 OK");
   else
      Put_line("Test P1 KO");
   end if;
   --Execution P4a
   if test_P4a_grille then
      Put_Line("Test P4a OK");
   else
      Put_line("Test P4b KO");
   end if;
   --Execution Exception_PileGrille_Pleine
   if not test_Exception_PileGrille_Pleine then
      Put_Line("Test Exception_PileGrille_Pleine OK");
   else
      Put_line("Test Exception_PileGrille_Pleine KO");
   end if;
   --Execution test_Exception_PileGrille_Videa
   if test_Exception_PileGrille_Videa then
      Put_Line("Test Exception_PileGrille_Videa OK");
   else
      Put_line("Test Exception_PileGrille_Videa KO");
   end if;
   --Execution test_Exception_PileGrille_Videb
   if test_Exception_PileGrille_Videb then
      Put_Line("Test Exception_PileGrille_Videb OK");
   else
      Put_line("Test Exception_PileGrille_Videb KO");
   end if;
   
   
      
end run_tests_TAD_Pile_Grille;
